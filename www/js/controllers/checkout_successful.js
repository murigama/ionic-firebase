/**
 * Created by fabior on 25/04/16.
 */
angular.module('starter.controllers')
    .controller('ClientCheckoutSuccessful',['$scope','$state','$cart','$ionicLoading','$ionicPopup',
        function($scope,$state,$cart,$ionicLoading,$ionicPopup){
            var cart = $cart.get();
            $scope.items = cart.items;
            $scope.total = cart.total;
            $cart.clear();
            
            $scope.openListOrder = function () {
                
            }

    }]);
