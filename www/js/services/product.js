angular.module('starter.services')
        .factory('Product',['$resource','appConfig',function ($resource,appConfig) {
            return $resource(appConfig.baseUrl + '/products',{},{
                query:{
                    isArray:true
                }
            });
        }]);